import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen: done
/// - Show confirm logout dialog before logout user.
/// - Go to my notes screen when click My Notes button.

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Future<_DataModel> _futureBuilder;
  _DataModel _dataModel;

  Future<void> _onLogoutButtonPressedAsync() async {
    showDialog(
        context: context,
        builder: (_) => new AlertDialog(
              title: new Text("Confirmation"),
              content: new Text("Are you sure you want to logout?"),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                FlatButton(
                  child: Text('Logout'),
                  onPressed: () async {
                    // Clear data and go to login screen.
                    final sharedPref = await SharedPreferences.getInstance();

                    await sharedPref.clear();

                    Navigator.of(context).pushNamedAndRemoveUntil(
                        AppRouter.login, (route) => false);
                  },
                )
              ],
            ));
  }

  void _onMyNotesButtonPressed() {
    // Which type of navigator to use?
    Navigator.of(context).pushNamed(AppRouter.myNoteList);
    // Navigator.of(context).pushReplacementNamed(AppRouter.myNoteList);
    // Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.myNoteList, (route) => false);
  }

  Future<_DataModel> _getScreenDataAsync() async {
    final sharedPref = await SharedPreferences.getInstance();

    _dataModel = _DataModel();
    _dataModel.userEmail = sharedPref.getString(SharedPreferenceKeys.userEmail);

    return _dataModel;
  }

  @override
  void initState() {
    _futureBuilder = _getScreenDataAsync();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
        actions: [
          IconButton(
            icon: Icon(Icons.logout),
            tooltip: 'Logout',
            onPressed: _onLogoutButtonPressedAsync,
          ),
        ],
      ),
      body: FutureBuilder<_DataModel>(
        initialData: null,
        future: _futureBuilder,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            return _content();
          }

          return Center(
            child: Text('Error'),
          );
        },
      ),
    );
  }

  Widget _content() {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(appConst.kDefaultPadding),
          child: Text('Hello ${_dataModel.userEmail}!'),
        ),
        Expanded(
          child: Center(
            child: ElevatedButton(
              child: Text('My Notes'),
              onPressed: _onMyNotesButtonPressed,
            ),
          ),
        ),
      ],
    );
  }
}

class _DataModel {
  String userEmail;
}
