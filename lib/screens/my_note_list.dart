import 'package:alcodes_on_board_flutter/constants/NoteDataHolder.dart';
import 'package:alcodes_on_board_flutter/screens/my_note_details.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../app_router.dart';
import '../constants/NoteDataHolder.dart';

/// TODO Issue on this screen: done
/// - Show list of my notes. Each list item need contain
///   note title on left, and Delete button on right.
/// - Click list item go to my note detail page.

class MyNoteList extends StatefulWidget {
  @override
  _MyNoteListState createState() => _MyNoteListState();
}

class _MyNoteListState extends State<MyNoteList> {
  int _noteListLength;

  final List<NoteList> noteList = [
    NoteList("1", "Note1", "This is Content1"),
    NoteList("2", "Note2", "This is Content2"),
    NoteList("3", "Note3", "This is Content3"),
    NoteList("4", "Note4", "This is Content4"),
    NoteList("5", "Note5", "This is Content5"),
    NoteList("6", "Note6", "This is Content6"),
    NoteList("7", "Note7", "This is Content7")
  ];

  @override
  void initState() {
    _noteListLength = noteList.length;

    super.initState();
  }

  void _deleteNote(int index) {
    setState(() {
      // update the text
      noteList.removeAt(index);
      _noteListLength--;
    });
  }

  void _onTapNoteListItem() {
    //TODO can be removed
    Navigator.of(context).pushNamed(AppRouter.myNoteDetails);
    // Navigator.of(context).pushReplacementNamed(AppRouter.myNoteDetails);
    // Navigator.of(context).pushNamedAndRemoveUntil(AppRouter.myNoteDetails, (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Notes'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: _noteListLength,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 1.0, vertical: 1.0),
          child: Card(
            child: ListTile(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            MyNoteDetails(noteList: noteList[index])));
                print(noteList[index]
                    .title); //TODO testing only, remove this after finish
                //_onTapNoteListItem(); //Goes to the respective note
              },
              title: Text(noteList[index].title),
              trailing: FlatButton(
                child: Text("Delete"),
                onPressed: () {
                  print("Delete " +
                      noteList[index]
                          .title); //TODO testing only, remove this after finish
                  _deleteNote(index);
                },
              ),
            ),
          ),
        ),
      ),
    );

    /*SingleChildScrollView(
            child: new GestureDetector(
                onTap: _onTapNoteListItem,
                child: Column(
                  children: noteList
                      .map((note) => Container(
                            //Maybe can try out card
                            margin: EdgeInsets.fromLTRB(2.0, 8.0, 2.0, 8.0),
                            padding: EdgeInsets.all(20.0),
                            color: Colors.grey[400],
                            width: double.infinity,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text(note), //Variable use ${note}.title
                                FlatButton(
                                    onPressed: () {}, child: Text("Delete")),
                              ],
                            ),
                          ))
                      .toList(),
                )))*/
  }
}
