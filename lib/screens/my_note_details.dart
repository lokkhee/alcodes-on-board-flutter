import 'package:alcodes_on_board_flutter/constants/NoteDataHolder.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
    as appConst;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyNoteDetails extends StatelessWidget {
  final NoteList noteList;

  MyNoteDetails({Key key, @required this.noteList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('My Note Details'),
          centerTitle: true,
        ),
        body: Column(children: [
          SizedBox(height: appConst.kDefaultPadding),
          Text(noteList.title, textScaleFactor: 2.0),
          Align(
            alignment: Alignment.centerRight,
          ),
          SizedBox(height: appConst.kDefaultPadding),
          Text(noteList.content),
          SizedBox(height: appConst.kDefaultPadding)
        ]));
  }
}
