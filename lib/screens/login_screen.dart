import 'package:alcodes_on_board_flutter/app_router.dart';
import 'package:alcodes_on_board_flutter/constants/app_constants/app_constants.dart'
as appConst;
import 'package:alcodes_on_board_flutter/constants/shared_preference_keys.dart';
import 'package:alcodes_on_board_flutter/utils/app_focus_helper.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

/// TODO Issue on this screen: done
/// - Password field should hide the inputs for security purpose.
/// - Login button is too small, make it fit the form width.
/// - Wrong login credential is showing as Toast, Toast will auto dismiss
///   and user will miss out the error easily, should use popup dialog.

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formModel = _FormModel();
  final _sampleEmail = 'alc@email.com';
  final _samplePassword = '123456';

  Future<void> _onSubmitFormAsync() async {
    // Hide keyboard.
    AppFocusHelper.instance.requestUnfocus();

    // Validate form and save inputs to form model.
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      if (_formModel.email == _sampleEmail &&
          _formModel.password == _samplePassword) {
        // Credential is ok.
        final sharedPref = await SharedPreferences.getInstance();

        await sharedPref.setString(
            SharedPreferenceKeys.userEmail, _formModel.email);

        Navigator.of(context)
            .pushNamedAndRemoveUntil(AppRouter.home, (route) => false);
      } else {
        // Wrong login credential.
        showDialog(
            context: context,
            builder: (_) =>
            new AlertDialog(
              title: new Text("Invalid Login Credential"),
              content: new Text("Invalid Email or Password entered."),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ));
        //Fluttertoast.showToast(msg: 'Invalid email or password.');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(appConst.kDefaultPadding),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                keyboardType: TextInputType.emailAddress,
                textInputAction: TextInputAction.next,
                onSaved: (newValue) => _formModel.email = newValue,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                decoration: InputDecoration(
                  hintText: 'Email',
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text('Email: $_sampleEmail'),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              TextFormField(
                textInputAction: TextInputAction.done,
                onSaved: (newValue) => _formModel.password = newValue,
                onFieldSubmitted: (value) => _onSubmitFormAsync(),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Required field.';
                  }

                  return null;
                },
                obscureText: true,
                //Hide password
                decoration: InputDecoration(
                  hintText: 'Password',
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Text('Password: $_samplePassword'),
              ),
              SizedBox(height: appConst.kDefaultPadding),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton(
                  onPressed: _onSubmitFormAsync,
                  child: Text('Login'),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _FormModel {
  String email;
  String password;
}
