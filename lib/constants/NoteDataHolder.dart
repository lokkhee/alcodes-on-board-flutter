class NoteList {
  String _id;
  String _title;
  String _content;

  NoteList(this._id, this._title, this._content);

  String get content => _content;

  set content(String value) {
    _content = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }
}
