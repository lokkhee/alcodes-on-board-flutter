import 'package:flutter/material.dart';

const kPrimaryColor = Colors.blue;
const kAccentColor = Colors.blueGrey;
const kScreenBackgroundColor = Colors.white;

const kTextColor = Colors.black;

const kAppBarBackgroundColor = Colors.blue;
const kAppBarTextColor = Colors.white;
